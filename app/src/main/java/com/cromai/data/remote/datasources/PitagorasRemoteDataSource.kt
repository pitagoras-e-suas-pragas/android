package com.cromai.data.remote.datasources

import com.cromai.NetworkConstants
import com.cromai.data.remote.ServiceGenerator
import com.cromai.data.remote.models.request.PitagorasRequest
import com.cromai.data.remote.services.PitagorasService

object PitagorasRemoteDataSource {
    private var mService = ServiceGenerator.createService(serviceClass = PitagorasService::class.java, url = NetworkConstants.BASE_URL)


    fun findHypotenuse(pitagorasRequest: PitagorasRequest) = mService.findHypotenuse(pitagorasRequest)

}