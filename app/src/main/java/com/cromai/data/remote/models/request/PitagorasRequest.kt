package com.cromai.data.remote.models.request

data class PitagorasRequest(
    var cat_op: Long,
    var cat_adj: Long
)