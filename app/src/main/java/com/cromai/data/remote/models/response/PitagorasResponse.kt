package com.cromai.data.remote.models.response


data class PitagorasResponse(
    val cat_adj: Long,
    val cat_op: Long,
    val hip: Double
)