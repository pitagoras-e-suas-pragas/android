package com.cromai.data.remote.services

import com.cromai.data.remote.models.request.PitagorasRequest
import com.cromai.data.remote.models.response.PitagorasResponse
import io.reactivex.Single
import retrofit2.http.*

interface PitagorasService {


    @POST("calcula")
    fun findHypotenuse(@Body pitagorasRequest: PitagorasRequest): Single<PitagorasResponse>

}