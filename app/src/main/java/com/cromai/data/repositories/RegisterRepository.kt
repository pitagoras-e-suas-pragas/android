package com.cromai.data.repositories

import com.cromai.data.remote.datasources.PitagorasRemoteDataSource
import com.cromai.data.remote.models.request.PitagorasRequest


object PitagorasRepository {

    private val mRemoteDataSource = PitagorasRemoteDataSource

    fun findHypotenuse(pitagorasRequest: PitagorasRequest) = mRemoteDataSource.findHypotenuse(pitagorasRequest)
}