package com.cromai.ui.splash

import android.content.Context
import android.os.Bundle
import android.os.Handler
import com.cromai.R
import com.cromai.ui.base.BaseActivity
import com.cromai.ui.main.createMainIntent

class SplashActivity : BaseActivity() {

    private lateinit var mContext: Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        mContext = this

        Handler().postDelayed({
            finish()
            startActivity(createMainIntent())
        }, 600)
    }

}