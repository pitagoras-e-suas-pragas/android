package com.cromai.ui.result

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import com.cromai.Constants
import com.cromai.R
import com.cromai.ui.base.BaseActivity
import com.cromai.ui.widgets.setVisible
import kotlinx.android.synthetic.main.activity_result.*
import org.jetbrains.anko.intentFor
import kotlin.math.abs
import kotlin.math.log10
import kotlin.math.pow
import kotlin.math.roundToInt

class ResultActivity : BaseActivity() {

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        val hypotenuse = (intent.getDoubleExtra(Constants.EXTRA_RESULT, 0.0))

        val hypotenuseLength = hypotenuse.length()

        var cromaiLink = ""

        var cromaiProduct = ""

        var text = ""

        var valueConverted = 0.0

        val unit = when (hypotenuseLength) {
            1 ->{
                "m"
            }
            2 -> {
                "dm"
            }
            3 -> {
                "cm"
            }
            5 -> {
                "dm"
            }
            6 -> {
                "cm"
            }
            else -> {
                "mm"
            }

        }

        hypotenuseTv.text = "Hipotenusa: $hypotenuse $unit"

        if (hypotenuseLength > 0) {
            viewGroup.setVisible(true)
            when {
                hypotenuseLength > 7 -> {
                    cromaiLink = "https://www.cromai.com/scan"
                    webviewBtn.setText("Conheça o Scan")
                    valueConverted = ((hypotenuse / (Math.pow(10.0, 6.0)) * 10).roundToInt() / 10.0).toDouble()
                    body1Tv.text = "As pragas estão ocupando apróximadamente = $valueConverted Quilômetros da sua lavoura"
                    body2Tv.text = "De acordo com minhas análises, possivelmente a praga atingiu quase toda sua fazenda, por isso, recomendo utilizar o Cromai Scan para descobrir o tipo de praga e resolver seu problema."
                    pictureIv.setBackgroundResource(R.drawable.scan)
                }
                hypotenuseLength > 4 -> {
                    cromaiLink = "https://www.cromai.com/sentinel"
                    webviewBtn.setText("Conheça o Sentinel")
                    valueConverted = ((hypotenuse / (10.0.pow(hypotenuseLength - 1.0)) * 10).roundToInt() / 10.0)
                    body1Tv.text = "As pragas estão ocupando apróximadamente = $valueConverted Quilômetros da sua lavoura"
                    body2Tv.text = "De acordo com minhas análises, possivelmente a praga atingiu boa parte da sua lavoura, por isso, recomendo utilizar o Cromai Sentinel para descobrir o tipo de praga e resolver seu problema."
                    pictureIv.setBackgroundResource(R.drawable.sentinel)
                }
                else -> {
                    cromaiLink = "https://www.cromai.com/capture"
                    webviewBtn.setText("Conheça o Capture")
                    valueConverted = ((hypotenuse / (10.0.pow(hypotenuseLength - 1.0)) * 10).roundToInt() / 10.0)
                    body1Tv.text = "As pragas estão ocupando apróximadamente = $valueConverted Metros da sua lavoura"
                    body2Tv.text = "De acordo com minhas análises, possivelmente a praga atingiu apenas uma de suas plantas e, por isso, recomendo utilizar o Cromai Capture para descobrir o tipo de praga e resolver seu problema."
                    pictureIv.setBackgroundResource(R.drawable.capture)
                }
            }
        }


        webviewBtn.setOnClickListener {
            val browserIntent =
                Intent(Intent.ACTION_VIEW, Uri.parse(cromaiLink))
            startActivity(browserIntent)
        }


    }

}

fun Context.createResultIntent(hip: Double): Intent =
    intentFor<ResultActivity>(Constants.EXTRA_RESULT to hip)

fun Double.length() = when(this) {
    0.0 -> 0
    else -> log10(abs(toDouble())).toInt() + 1
}