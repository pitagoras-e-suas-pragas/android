package com.cromai.ui.main

import com.cromai.data.remote.models.request.PitagorasRequest
import com.cromai.data.remote.models.response.PitagorasResponse
import com.cromai.ui.base.BasePresenter
import com.cromai.ui.base.BaseView

interface MainContract {

    interface View : BaseView<Presenter> {
        fun displayHypotenuseScreen(pitagorasResponse: PitagorasResponse)
        fun displayLoading(loading: Boolean)
        fun displayError(msg: String?)
    }

    interface Presenter : BasePresenter<View> {
        fun onBtnClicked(pitagorasRequest: PitagorasRequest)
    }

}