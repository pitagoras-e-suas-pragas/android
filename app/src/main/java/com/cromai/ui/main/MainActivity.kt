package com.cromai.ui.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.annotation.StringRes
import com.cromai.R
import com.cromai.data.remote.models.request.PitagorasRequest
import com.cromai.data.remote.models.response.PitagorasResponse
import com.cromai.ui.base.BaseActivity
import com.cromai.ui.result.createResultIntent
import com.cromai.ui.widgets.AppTextInputLayout
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast

class MainActivity : BaseActivity(), MainContract.View {

    private lateinit var mPresenter: MainContract.Presenter
    private lateinit var mContext: Context
    private lateinit var mFieldsList: MutableList<AppTextInputLayout>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setFields()
        setPresenter()
        setListener()
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.detachView()
    }


    private fun setFields() {
        mFieldsList = mutableListOf(catOpAtil, catAdjAtil)
    }

    override fun setPresenter() {
        mContext = this
        mPresenter = MainPresenter()
        mPresenter.attachView(this)
    }

    private fun setListener() {
        sendBtn.setOnClickListener {
            var isFieldsValid = true
            mFieldsList.forEach {
                if (!it.isFieldValid) {
                    isFieldsValid = false
                    return@forEach
                }
            }
            if (!isFieldsValid) {
                showToast(str(R.string.error_incorrect_fields))
            } else {
                mPresenter.onBtnClicked(PitagorasRequest(cat_op = catOpAtil.text.trim().toLong(), cat_adj = catAdjAtil.text.trim().toLong()))
            }
        }
    }

    override fun displayError(msg: String?) {
        msg?.let {
            toast(it)
        }
    }

    override fun displayLoading(loading: Boolean) {
        sendBtn.setLoading(loading)
    }

    override fun displayHypotenuseScreen(pitagorasResponse: PitagorasResponse) {
        startActivity(createResultIntent(pitagorasResponse.hip))
    }
}


fun Context.createMainIntent(): Intent {
    return intentFor<MainActivity>()
}

fun Context.str(@StringRes id: Int, vararg formatArgs: Any?): String {
    return getString(id, formatArgs)
}