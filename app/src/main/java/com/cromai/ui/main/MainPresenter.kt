package com.cromai.ui.main

import com.cromai.data.remote.models.request.PitagorasRequest
import com.cromai.data.repositories.PitagorasRepository
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

class MainPresenter : MainContract.Presenter {

    private var mView: MainContract.View? = null
    private var mComposite = CompositeDisposable()

    override fun attachView(mvpView: MainContract.View?) {
        mView = mvpView
    }

    override fun detachView() {
        mView = null
        mComposite.dispose()
    }

    override fun onBtnClicked(pitagorasRequest: PitagorasRequest) {
        mView?.displayLoading(true)
        PitagorasRepository.findHypotenuse(pitagorasRequest).singleSubscribe(
            onSuccess = {
                mView?.displayLoading(false)
                mView?.displayHypotenuseScreen(it)
            },
            onError = {
                mView?.displayLoading(false)
                mView?.displayError("Não foi possível fazer a requisição")
            }
        )
    }

}

fun <T> Single<T>.singleSubscribe(onSuccess: (t: T) -> Unit, onError: (e: Throwable) -> Unit) =
    this.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeWith(object : DisposableSingleObserver<T>() {
            override fun onSuccess(t: T) {
                onSuccess(t)
            }

            override fun onError(e: Throwable) {
                onError(e)
            }
        })