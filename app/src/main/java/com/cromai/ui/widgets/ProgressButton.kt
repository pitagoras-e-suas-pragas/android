package com.cromai.ui.widgets

import android.content.Context
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.os.Build
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import androidx.annotation.ColorRes
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import com.cromai.R
import kotlinx.android.synthetic.main.progress_button.view.*
import org.jetbrains.anko.backgroundResource

class ProgressButton @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyle: Int = 0) : FrameLayout(context, attrs, defStyle) {

    init {
        LayoutInflater.from(context).inflate(R.layout.progress_button, this, true)
        attrs?.let {
            val typedArray = context.obtainStyledAttributes(it, R.styleable.ProgressButton, 0, 0)

            //ENABLED
            val enabled = typedArray.getBoolean(R.styleable.ProgressButton_btnEnabled, true)
            isEnabled = enabled

            //BACKGROUND
            val backgroundRes = typedArray.getResourceId(R.styleable.ProgressButton_btnBackground, -1)
            if (backgroundRes != -1) backgroundResource = backgroundRes

            //ICON
            val icon = typedArray.getResourceId(R.styleable.ProgressButton_btnIcon, -1)
            if (icon != -1) progressButtonImageView.setImageDrawable(AppCompatResources.getDrawable(context, icon))

            //PROGRESS BAR
            val progressColor = typedArray.getResourceId(R.styleable.ProgressButton_btnProgressColor, -1)
            if (progressColor != -1) {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    val wrapDrawable = DrawableCompat.wrap(progressButtonProgressBar.indeterminateDrawable)
                    DrawableCompat.setTint(wrapDrawable, ContextCompat.getColor(getContext(), android.R.color.holo_green_light))
                    progressButtonProgressBar.indeterminateDrawable = DrawableCompat.unwrap<Drawable>(wrapDrawable)
                } else {
                    progressButtonProgressBar.indeterminateDrawable.setColorFilter(context.getColorRes(progressColor), PorterDuff.Mode.SRC_IN)
                }
            }

            val rippleBackgroundRes = typedArray.getResourceId(R.styleable.ProgressButton_customSelectableItemBackground, -1)
            foreground = if (rippleBackgroundRes != -1) {
                AppCompatResources.getDrawable(context, rippleBackgroundRes)
            } else {
                val typedValue = TypedValue()
                context.theme.resolveAttribute(R.attr.selectableItemBackground, typedValue, true)
                AppCompatResources.getDrawable(context, typedValue.resourceId)
            }

            //TEXT
            progressButtonTextView.text = typedArray.getString(R.styleable.ProgressButton_btnText)
            val textColorRes = typedArray.getResourceId(R.styleable.ProgressButton_btnTextColor, -1)
            val textSizeRes = typedArray.getDimensionPixelSize(R.styleable.ProgressButton_btnTextSize, -1)
            val textAllCaps = typedArray.getBoolean(R.styleable.ProgressButton_btnAllCaps, true)
            if (textColorRes != -1) progressButtonTextView.setTextColor(ContextCompat.getColor(context, textColorRes))
            if (textSizeRes != -1) setTextSize(textSizeRes)
            progressButtonTextView.setAllCaps(textAllCaps)
//            progressButtonTextView.setTypeface(progressButtonTextView.typeface, Typeface.BOLD)

            typedArray.recycle()
        }
    }

    fun setDimens(height: Int, width: Int) {
        layoutParams.height = height
        layoutParams.width = width
    }

    fun setText(text: String) {
        progressButtonImageView.setVisible(false)
        progressButtonTextView.setVisible(true)
        progressButtonTextView.text = text
    }

    fun setDrawable(drawable: Drawable) {
        progressButtonTextView.setVisible(false)
        progressButtonImageView.setVisible(true)
        progressButtonTextView.text = ""
        progressButtonImageView.setImageDrawable(drawable)
    }

    fun setTextColor(@ColorRes colorRes: Int) {
        progressButtonTextView.setTextColor(ContextCompat.getColor(context, colorRes))
        invalidate()
        requestLayout()
    }

    fun setBackground(backgroundRes: Int) {
        backgroundResource = backgroundRes
    }

    fun setBtnBackgroundColor(colorRes: Int) {
        if (colorRes != -1 && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            backgroundResource = colorRes
        }
    }

    fun setBtnEnabled(enabled: Boolean) {
        isEnabled = enabled
    }

    fun setLoading(loading: Boolean) {
        isEnabled = !loading
        progressButtonProgressBar.setVisible(loading)
        if (progressButtonTextView.text.isNullOrBlank()) {
            progressButtonImageView.setVisible(!loading, true)
        } else {
            progressButtonTextView.setVisible(!loading, true)
        }
    }

    private fun setTextSize(size: Int) {
        progressButtonTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, size.toFloat())
        invalidate()
        requestLayout()
    }
}


fun Context.getColorRes(idRes: Int): Int {
    return ContextCompat.getColor(this, idRes)
}

fun View.setVisible(visible: Boolean, useInvisible: Boolean = false) {
    visibility = when {
        visible -> View.VISIBLE
        useInvisible -> View.INVISIBLE
        else -> View.GONE
    }
}