package com.cromai.ui.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.Nullable;

import com.cromai.R;
import com.google.android.material.textfield.TextInputLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Objects;
import java.util.regex.Pattern;


public class AppTextInputLayout extends TextInputLayout {
    private final int TYPE_CUSTOM = 0;
    private final int TYPE_NAME = 1;
    private final int TYPE_MAIL = 2;
    private final int TYPE_DATE = 3;
    private final int TYPE_PHONE = 4;
    private final int TYPE_CEP = 5;
    private final int TYPE_CREDIT_CARD = 6;
    private final int TYPE_PASSWORD = 7;
    private final int TYPE_MATCHING = 8;
    private final int TYPE_CPF = 9;
    private final int TYPE_CNPJ = 10;
    private final int TYPE_CREDIT_CARD_DATE = 11;

    private int mInputType;
    private boolean mFieldNeedsValidation;
    private boolean mEmptinessIsValid;
    private String mEmptyErrorText;
    private String mInvalidErrorText;
    private int mMinLength;
    private AppTextInputLayout mMatchingReference;
    private String mMask = "";
    private TypedArray mValues;

    private String mRegex;

    private EditText mEditText;

    public AppTextInputLayout(Context context) {
        super(context);
    }

    public AppTextInputLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        mValues = context.getTheme().obtainStyledAttributes(attrs, R.styleable.AppTextInputLayout, 0, 0);
        initialize(mValues);
    }

    public AppTextInputLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mValues = context.getTheme().obtainStyledAttributes(attrs, R.styleable.AppTextInputLayout, 0, 0);
        initialize(mValues);
    }

    private void initialize(TypedArray values) {
        mFieldNeedsValidation = values.getBoolean(R.styleable.AppTextInputLayout_validation, true);
        mEmptyErrorText = values.getString(R.styleable.AppTextInputLayout_emptyErrorText) == null ? "" : values.getString(R.styleable.AppTextInputLayout_emptyErrorText);
        mInvalidErrorText = values.getString(R.styleable.AppTextInputLayout_invalidErrorText) == null ? "" : values.getString(R.styleable.AppTextInputLayout_invalidErrorText);
        mInputType = values.getInt(R.styleable.AppTextInputLayout_inputTextType, TYPE_CUSTOM);
        mMinLength = values.getInt(R.styleable.AppTextInputLayout_minLength, 0);
        mMask = values.getString(R.styleable.AppTextInputLayout_customMask) == null ? "" : values.getString(R.styleable.AppTextInputLayout_customMask);
        mEmptinessIsValid = values.getBoolean(R.styleable.AppTextInputLayout_emptinessIsValid, false);
        mRegex = values.getString(R.styleable.AppTextInputLayout_pattern) == null ? "" : values.getString(R.styleable.AppTextInputLayout_pattern);
        values.recycle();
    }

    private boolean mIsInitialized = false;

    @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params) {
        super.addView(child, index, params);
        if (getEditText() != null && !mIsInitialized) {
            mIsInitialized = true;
            setInputType();
        }
    }

    public void setParams(int inputTextType, String hint, boolean emptinessValid, int minLength) {
        if (inputTextType >= 0) {
            mInputType = inputTextType;
            setInputType();
        }

        if (hint != null) {
            setHint(hint);
        }

        mEmptinessIsValid = emptinessValid;
        mMinLength = minLength;

        if (mEmptyErrorText.isEmpty())
            mEmptyErrorText = getContext().getString(R.string.app_text_input_layout_empty_field, getHint().toString());
    }

    public void setEditTextInputType(int inputType) {
        getEditText().setInputType(inputType);
    }

    private void setInputType() {
        mEditText = getEditText();
        setEditTextListener(mEditText);
        switch (mInputType) {
            case TYPE_CUSTOM:
                break;
            case TYPE_NAME:
                getEditText().setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);
                break;
            case TYPE_MAIL:
                getEditText().setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                break;
            case TYPE_PASSWORD:
                getEditText().setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                break;
            case TYPE_MATCHING:
                getEditText().setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                break;
            case TYPE_DATE:
                getEditText().setInputType(InputType.TYPE_CLASS_NUMBER);
                break;
            case TYPE_PHONE:
                getEditText().setInputType(InputType.TYPE_CLASS_NUMBER);
                break;
            case TYPE_CEP:
                getEditText().setInputType(InputType.TYPE_CLASS_NUMBER);
                break;
            case TYPE_CREDIT_CARD:
                break;
            case TYPE_CPF:
                getEditText().setInputType(InputType.TYPE_CLASS_NUMBER);
                break;
            case TYPE_CNPJ:
                getEditText().setInputType(InputType.TYPE_CLASS_NUMBER);
                break;
            case TYPE_CREDIT_CARD_DATE:
                getEditText().setInputType(InputType.TYPE_CLASS_NUMBER);
                break;
        }
        if (!mMask.isEmpty()) {
            addMask(mMask);
        }
    }

    private void addMask(String mask) {
//        mEditText.addTextChangedListener(TextMask.insert(mask, mEditText));
    }

    private void setEditTextListener(final EditText editTextListener) {
        editTextListener.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (mFieldNeedsValidation) {
                    if (b) {
                        setErrorEnabled(false);
                    } else {
                        validate();
                    }
                }
            }
        });
    }

    public void validate() {
        if (mFieldNeedsValidation) {
            //Type validations
            boolean isFieldValid = true;
            switch (mInputType) {
                case TYPE_CUSTOM:
                case TYPE_NAME:
                case TYPE_PHONE:
                case TYPE_CEP:
                case TYPE_CREDIT_CARD:
                case TYPE_PASSWORD:
                    break;
                case TYPE_DATE:
                    isFieldValid = isDateValid();
                    break;
                case TYPE_CREDIT_CARD_DATE:
                    isFieldValid = isCardDateValid();
                    break;
                case TYPE_CNPJ:
//                    isFieldValid = isCnpjValid();
                    break;
                case TYPE_MAIL:
//                    isFieldValid = isMailValid();
                    break;
                case TYPE_MATCHING:
                    isFieldValid = isMatchingValid();
                    break;
                case TYPE_CPF:
//                    isFieldValid = isCpfValid();
                    break;
            }

            if (ignoreValidation())
                return;

            //Emptiness validation
            if (!isNotEmpty()) {
                setEmptyErrorText();

            } else if (mEmptinessIsValid && getText().isEmpty()) {
                setErrorEnabled(false);
            } else if (!isFieldValid || !isPatternValid()) {
                setInvalidErrorText();
            } else if (!isLengthValid()) {
                if (mMask.isEmpty()) {
                    showMinLengthErrorText();
                } else {
                    setInvalidErrorText();
                }
            } else {
                //if got here it's because field is valid
                setErrorEnabled(false);
            }

        }
    }

    private boolean ignoreValidation() {
        return mEditText.getText().toString().isEmpty() && mEmptinessIsValid;
    }

    private boolean isDateValid() {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        try {
            format.setLenient(false);
            format.parse(mEditText.getText().toString());
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

    private boolean isCardDateValid() {
        SimpleDateFormat format = new SimpleDateFormat("MM/yy");
        try {
            format.setLenient(false);
            format.parse(mEditText.getText().toString());
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

    private boolean isPatternValid() {
        //Returns true only if there' a regex and the Pattern matches
        return mRegex.isEmpty() || Pattern.matches(mRegex, mEditText.getText());
    }

    private boolean isLengthValid() {
        if (!mMask.isEmpty()) {

//            if (mMask.equals(TextMask.PHONE_MASK)) {
//                return hasMinLengthOrMore(TextMask.PHONE_MASK.length() - 1) || hasMinLengthOrMore(TextMask.CEL_PHONE_MASK.length());
//            }

            mMinLength = mMask.length();
        }

        return hasMinLengthOrMore(mMinLength);
    }

    private boolean hasMinLengthOrMore(int minLength) {
        return minLength == 0 || mEditText.getText().length() >= minLength;
    }

//    private boolean isCpfValid() {
//        return IsCpf.isValid(mEditText.getText().toString());
//    }

//    private boolean isCnpjValid() {
//        return IsCnpj.isValid(mEditText.getText().toString());
//    }

    private boolean isNotEmpty() {
        return mEmptinessIsValid || !mEditText.getText().toString().trim().isEmpty();
    }

    public boolean isFieldValid() {
        validate();
        return !isErrorEnabled();
    }

//    private boolean isMailValid() {
//        return IsEmail.isValid(mEditText.getText().toString());
//    }

    private void setEmptyErrorText() {
        setError(mEmptyErrorText.isEmpty() ? getContext().getString(R.string.app_text_input_layout_empty_field, Objects.requireNonNull(getHint()).toString()) : mEmptyErrorText);
    }

    private void setInvalidErrorText() {
        setError(mInvalidErrorText.isEmpty() ? getContext().getString(R.string.app_text_input_layout_invalid_field, Objects.requireNonNull(getHint()).toString()) : mInvalidErrorText);
    }

    private void showMinLengthErrorText() {
        setError(mInvalidErrorText.isEmpty() ? getContext().getString(R.string.app_text_input_layout_password_field, getHint().toString(), mMinLength) : mInvalidErrorText);
    }

    private boolean isMatchingValid() {
        try {
            return mMatchingReference.getText().equals(mEditText.getText().toString());
        } catch (NullPointerException e) {
            return true;
        }
    }

    public void setMatchingReference(AppTextInputLayout matchingReference) {
        mMatchingReference = matchingReference;
    }

    public void setText(String text) {
        if (getEditText() != null) {
            getEditText().setText(text);
        }
    }

    public String getText() {
        return getEditText().getText().toString();
    }

//    public String getUnmaskedText() {
//        return TextMask.unmask(getText());
//    }

    @Override
    public void setOnClickListener(@Nullable View.OnClickListener l) {
        mEditText.setOnClickListener(l);
        mEditText.setFocusable(false);
        mEditText.setClickable(true);
        setClickable(true);
        setFocusable(true);
        mEditText.setEnabled(true);
        super.setOnClickListener(l);
    }
}
