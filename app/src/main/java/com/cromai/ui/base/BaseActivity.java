package com.cromai.ui.base;

import android.annotation.SuppressLint;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.Toolbar;

import com.cromai.R;
import com.cromai.utils.ViewUtil;

import java.util.Objects;



public abstract class BaseActivity extends AppCompatActivity {

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //FIX ORIENTATION TO PORTRAIT
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    //TOOLBAR METHODS
    public void setToolbar(String title, boolean displayHomeAsUpEnabled) {
        setToolbar(title);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(displayHomeAsUpEnabled);
    }

    //ACTION BAR METHODS
    public void setToolbar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);
    }

    //ACTION BAR METHODS
    public void setToolbar(String title, int color) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(color);
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);
    }

    //ACTION BAR METHODS
    public void setToolbar(String title, int color, boolean displayHomeAsUpEnabled) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(color);
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(displayHomeAsUpEnabled);
        final Drawable upArrow = AppCompatResources.getDrawable(this, R.drawable.ic_back);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
    }

    //TOAST METHODS
    public void showToast(String string) {
        Toast.makeText(this, string, Toast.LENGTH_SHORT).show();
    }

    public void showLongToast(String string) {
        Toast.makeText(this, string, Toast.LENGTH_LONG).show();
    }

//    //PROGRESS DIALOG METHODS
//    private void showProgressDialog() {
//        mProgressDialog = (mProgressDialog == null) ? DialogHelper.createProgressDialog(this) : mProgressDialog;
//        mProgressDialog.show();
//    }
//
//    private void dismissProgressDialog() {
//        if (mProgressDialog != null) {
//            mProgressDialog.dismiss();
//        }
//    }

    //KEYBOARD METHODS
    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            ViewUtil.hideKeyboard(this);
        }
    }

    //Navigation Bar
    public void hideNavigationBar() {
        View decorView = getWindow().getDecorView();
        // Hide both the navigation bar and the status bar.
        // SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
        // a general rule, you should design your app to hide the status bar whenever you
        // hide the navigation bar.
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }

    //MENU METHODS
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //this is used because when user hits home button the previous view is reconstructed
                //and when back button (at navbar) is pressed this doesn't happen,
                //so this makes the previous view never reconstructed when home is hit.
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
