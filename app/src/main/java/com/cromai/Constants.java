package com.cromai;

/**
 * Created by rafaeliglesiasnovak on 10/02/2018.
 */

public class Constants {
    public static final String PACKAGE_NAME = BuildConfig.APPLICATION_ID;

    public static final String GOOGLE_PLAY_URL = "https://play.google.com/store/apps/details?menu_id=" + PACKAGE_NAME;

    public static final String EXTRA_RESULT = PACKAGE_NAME + ".EXTRA_RESULT";


}
